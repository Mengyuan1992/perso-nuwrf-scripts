#!/bin/tcsh

# Change from here!
set res=25
set outdir=/home/z3368490/DATA15/RegridLIS_out/25KM/

set indir=/home/z3368490/DATA15/LIS_PARAMS/UMD/1KM/
set indir100=/home/z3368490/DATA15/LIS_PARAMS/UMD/100KM/
# End change

module load intel
module load ncl

ifort -convert big_endian -assume byterecl landmask_from_1km.F90 -o landmask_from_1km
ifort -convert big_endian -assume byterecl domdata_from_1km.F90 -o domdata_from_1km
ifort -convert big_endian -assume byterecl domdata_from_100km.F90 -o domdata_from_100km
ifort -convert big_endian -assume byterecl soildata_from_1km.F90 -o soildata_from_1km
ifort -convert big_endian -assume byterecl slopedata_from_1km.F90 -o slopedata_from_1km
ifort -convert big_endian -assume byterecl soildata_nofill_from_1km.F90 -o soildata_nofill_from_1km
ifort -convert big_endian -assume byterecl vegIGBP_from_1km.F90 -o vegIGBP_from_1km
ifort -convert big_endian -assume byterecl vegUMD_from_1km.F90 -o vegUMD_from_1km
ifort -convert big_endian -assume byterecl vegUSGS_from_1km.F90 -o vegUSGS_from_1km

set file=landmask_UMD.1gd4r
./landmask_from_1km ${res}  ${indir}${file} ${outdir}${file} ${outdir}landmask_UMD.1gd4r

set mask=${outdir}landmask_UMD.1gd4r

set file=landcover_IGBP_NCEP.1gd4r
./vegIGBP_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} 0
set file=landcover_UMD.1gd4r
./vegUMD_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} 0
set file=landcover_USGS.1gd4r
./vegUSGS_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} 0
set file=soiltexture_STATSGO-FAO.1gd4r
./soildata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} 
set file=albedo_NCEP.apr.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.aug.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.aut.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.dec.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.feb.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.jan.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.jul.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.jun.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.mar.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.may.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.nov.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.oct.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.sep.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.spr.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.sum.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=albedo_NCEP.win.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}

set file=clay_FAO.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} -0.99
set file=silt_FAO.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} -0.99
set file=sand_FAO.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} -0.99
set file=elev_GDAS-T170.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=elev_GDAS-T254.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=elev_GDAS-T382.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=elev_GDAS-T574.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=elev_GTOPO30.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}

set file=lai_AVHRR_CLIM.jan.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.feb.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.mar.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.apr.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.may.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.jun.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.jul.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.aug.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.sep.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.oct.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.nov.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=lai_AVHRR_CLIM.dec.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}

set file=gvf_NCEP.jan.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.feb.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.mar.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.apr.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.may.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.jun.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.jul.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.aug.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.sep.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.oct.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.nov.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.dec.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.MAX.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=gvf_NCEP.MIN.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}

set file=mxsnoalb_NCEP.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=slopetype_NCEP.1gd4r
./slopedata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask} 0
set file=soilcolor_FAO.1gd4r
./soildata_nofill_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=tbot_GDAS_6YR_CLIM.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}
set file=tbot_NCEP.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}

# LAI
ncl res=${res} outdir=\"${outdir}\" interp_MODIS_LAI_1km.ncl

# Albedo
set file=ccrc_soil_alb_int_short.1gd4r
./domdata_from_1km ${res} ${indir}${file} ${outdir}${file} ${mask}

# Princeton elev file
set file=elev_hydro1k_mean_1d.1gd4r
./domdata_from_100km ${res} ${indir100}${file} ${outdir}${file} ${mask}
